import React from 'react';
import {SafeAreaView} from 'react-native';
import Main from './src/components/Main';
import SplashScreen from 'react-native-splash-screen';
export default class App extends React.PureComponent {
  componentDidMount() {
    SplashScreen.hide();
  }

  render() {
    return (
      <SafeAreaView style={styles.body}>
        <Main />
      </SafeAreaView>
    );
  }
}

const styles = {
  body: {
    backgroundColor: 'white',
    flex: 1,
  },
};
