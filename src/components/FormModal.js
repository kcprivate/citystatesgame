import React from 'react';
import {Modal, Text, View, Pressable} from 'react-native';
import FormInput from './FormInput';
export default class FormModal extends React.PureComponent {
  setModalContent = () => {
    const {modalContent, actualPlayer, setPlayersCount, playersScore} =
      this.props;
    const {textCenter, modalText, winningPlayer, results} = styles;
    if (modalContent === 'beginning') {
      return (
        <>
          <Text style={textCenter}>Wprowadź liczbę graczy</Text>
          <FormInput
            singleInput
            setResult={setPlayersCount}
            type="playersCount"
            isNumber
          />
        </>
      );
    } else if (modalContent === 'round') {
      let txt = 'Kolej gracza nr. ' + actualPlayer;
      return <Text style={modalText}>{txt}</Text>;
    } else if (modalContent === 'end') {
      let winScore = [];
      winScore = playersScore;
      let winner = this.maxIndex(winScore) + 1;
      let txt = 'Wygrał gracz nr. ' + winner;
      return (
        <>
          <Text style={[modalText, winningPlayer]}>{txt}</Text>
          <Text style={modalText}>Punktacja: </Text>
          <View style={results}>{winScore.map(this.printArray)}</View>
        </>
      );
    }
  };

  printArray = (element, index) => {
    index += 1;
    let txt = 'Gracz nr. ' + index + ' zdobył ' + element + ' punktów.';
    return (
      <Text style={styles.modalText} key={index}>
        {txt}
      </Text>
    );
  };

  maxIndex = arr => {
    if (arr.length === 0) {
      return -1;
    }

    var max = arr[0];
    var maxIndex = 0;

    for (var i = 1; i < arr.length; i++) {
      if (arr[i] > max) {
        maxIndex = i;
        max = arr[i];
      }
    }
    return maxIndex;
  };

  setModalButtonText = () => {
    const {textCenter} = styles;
    const {modalContent} = this.props;
    if (modalContent === 'beginning') {
      return <Text style={textCenter}>Rozpocznij grę</Text>;
    } else if (modalContent === 'round') {
      return <Text style={textCenter}>Kolejna osoba</Text>;
    } else {
      return <Text style={textCenter}>Nowa gra</Text>;
    }
  };

  clearGame = () => {
    this.props.actualPlayer = 1;
    this.props.randomizeChar();
    this.props.setBeginning('beginning', 1);
    this.setModalContent();
    this.setModalButtonText();
    this.props.setModal(true);
  };
  render() {
    const {modalCentered, modalView, buttonClose} = styles;
    return (
      <View style={modalCentered}>
        <Modal
          animationType="slide"
          transparent={true}
          visible={this.props.isVisible}
          onRequestClose={() => {
            !this.props.isVisible;
          }}>
          <View style={modalCentered}>
            <View style={modalView}>
              {this.setModalContent()}
              <Pressable
                style={buttonClose}
                onPress={() => {
                  this.props.setModal(false);
                  if (this.props.modalContent === 'end') {
                    this.clearGame();
                  }
                }}>
                {this.setModalButtonText()}
              </Pressable>
            </View>
          </View>
        </Modal>
      </View>
    );
  }
}
const styles = {
  textCenter: {
    textAlign: 'center',
  },
  modalCentered: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  modalView: {
    width: '90%',
    backgroundColor: 'white',
    borderRadius: 20,
    padding: 20,
    justifyContent: 'center',
    borderWidth: 1,
    borderColor: 'black',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
  modalText: {
    textAlign: 'center',
    marginBottom: 10,
  },
  buttonClose: {
    width: '50%',
    alignSelf: 'center',
    textAlign: 'center',
    borderRadius: 5,
    padding: 20,
    borderWidth: 1,
    borderColor: 'black',
  },
  results: {
    textAlign: 'center',
    justifyContent: 'center',
    alignItems: 'center',
  },
  winningPlayer: {
    fontWeight: 'bold',
  },
};
