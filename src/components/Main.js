import React from 'react';
import {Pressable, View, Text} from 'react-native';
import FormInput from './FormInput';
import FormModal from './FormModal';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import Header from './Header';

export default class Form extends React.PureComponent {
  defaultState = {
    country: '',
    city: '',
    name: '',
    item: '',
    animal: '',
    plant: '',
  };
  state = {
    ...this.defaultState,
    modalVisible: true,
    gameState: 'beginning',
    playersCount: 0,
    actualPlayer: 1,
    playersFinalAnswers: [],
    playersScore: [],
    chosenChar: '',
  };

  componentDidMount() {
    this.randomChar();
  }

  setGameState = async () => {
    const {gameState, actualPlayer, playersCount} = this.state;
    if (gameState === 'beginning') {
      await this.setStateAsync({gameState: 'round'});
      await this.setStateAsync({actualPlayer: actualPlayer + 1});
    } else if (gameState === 'round' && actualPlayer < playersCount) {
      await this.setStateAsync({gameState: 'round'});
      await this.setStateAsync({actualPlayer: actualPlayer + 1});
    } else {
      this.checkCorrectAnswers();
      await this.setStateAsync({gameState: 'end'});
    }
  };

  setInputResult = (value, type) => {
    this.setState({
      [type]: value,
    });
  };

  addingAnswers = async () => {
    const {country, city, name, item, animal, plant} = this.state;
    if (this.state.gameState === 'beginning') {
      await this.setStateAsync({
        playersFinalAnswers: [],
        playersScore: [],
      });
    }
    let playerAnswers = [];
    playerAnswers.push(country);
    playerAnswers.push(city);
    playerAnswers.push(name);
    playerAnswers.push(item);
    playerAnswers.push(animal);
    playerAnswers.push(plant);
    await this.setStateAsync({
      playersFinalAnswers: [...this.state.playersFinalAnswers, playerAnswers],
    });
    this.setState({...this.defaultState});
  };

  setModalVisible = visible => {
    this.setState({modalVisible: visible});
  };

  setStateAsync(state) {
    return new Promise(resolve => {
      this.setState(state, resolve);
    });
  }

  randomChar = () => {
    let possible = 'abcdefghijklmnoprstuwyz';
    this.setState({
      chosenChar: possible.charAt(Math.floor(Math.random() * possible.length)),
    });
  };

  checkCorrectAnswers = async () => {
    for (let i = 0; i < this.state.playersCount; ++i) {
      await this.setStateAsync({
        playersScore: [...this.state.playersScore, 0],
      });
    }
    for (let i = 0; i < this.state.playersFinalAnswers[0].length; ++i) {
      let temp = [];
      for (let j = 0; j < this.state.playersCount; ++j) {
        temp.push(this.state.playersFinalAnswers[j][i]);
      }
      for (let j = 0; j < temp.length; ++j) {
        let check = [];
        check = Array.from(temp);
        check.splice(j, 1);
        if (
          this.state.playersFinalAnswers[j][i].toLowerCase().charAt(0) ===
          this.state.chosenChar
        ) {
          if (check.includes(this.state.playersFinalAnswers[j][i])) {
            let result = [];
            result = [...this.state.playersScore];
            result[j] += 10;
            await this.setStateAsync({
              playersScore: result,
            });
          } else {
            let result = [];
            result = [...this.state.playersScore];
            result[j] += 15;
            await this.setStateAsync({
              playersScore: result,
            });
          }
        } else if (this.state.playersFinalAnswers[j][i] === '') {
          let result = [];
          result = [...this.state.playersScore];
          result[j] += 0;
          await this.setStateAsync({
            playersScore: result,
          });
        } else {
          let result = [];
          result = [...this.state.playersScore];
          result[j] -= 20;
          await this.setStateAsync({
            playersScore: result,
          });
        }
      }
    }
  };

  setBeginning = (text, number) => {
    this.setState({
      gameState: text,
    });
    this.setState({
      actualPlayer: number,
    });
  };

  render() {
    var keys = {
      country: 'Państwo',
      city: 'Miasto',
      name: 'Imię',
      item: 'Rzecz',
      animal: 'Zwierzę',
      plant: 'Roślina',
    };
    return (
      <View style={styles.mainForm}>
        <Header
          chosenChar={this.state.chosenChar}
          actualPlayer={this.state.actualPlayer}
        />
        <KeyboardAwareScrollView>
          <FormModal
            isVisible={this.state.modalVisible}
            setModal={this.setModalVisible}
            modalContent={this.state.gameState}
            setBeginning={this.setBeginning}
            playersCount={this.state.playersCount}
            actualPlayer={this.state.actualPlayer}
            setPlayersCount={this.setInputResult}
            playersScore={this.state.playersScore}
            randomizeChar={this.randomChar}
          />
          {Object.entries(keys).map(e => {
            return (
              <FormInput
                key={e[0]}
                setResult={this.setInputResult}
                value={this.state[e[0]]}
                type={e[0]}>
                {e[1]}
              </FormInput>
            );
          })}
        </KeyboardAwareScrollView>
        <Pressable
          style={styles.btn}
          onPress={() => {
            this.addingAnswers();
            this.setGameState();
            this.setModalVisible(true);
          }}>
          <Text>Wyślij odpowiedzi</Text>
        </Pressable>
      </View>
    );
  }
}

const styles = {
  mainForm: {
    flex: 1,
  },
  btn: {
    alignSelf: 'center',
    textAlign: 'center',
    borderRadius: 2,
    margin: 10,
    padding: 10,
    paddingLeft: 30,
    paddingRight: 30,
    borderWidth: 1,
    borderColor: 'black',
  },
};
