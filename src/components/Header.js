import React from 'react';
import {Text, View} from 'react-native';

export default class Header extends React.PureComponent {
  render() {
    const {header, mainTitle, subTitle} = styles;
    let chosenChar = this.props.chosenChar,
      text = 'Wylosowana literka: ' + chosenChar.toUpperCase(),
      actuallyPlaying = 'Gracz nr: ' + this.props.actualPlayer;
    return (
      <View style={header}>
        <Text style={mainTitle}>Gra w Państwa-Miasta</Text>
        <Text style={subTitle}>{actuallyPlaying}</Text>
        <Text style={subTitle}>{text}</Text>
      </View>
    );
  }
}

const styles = {
  header: {
    marginBottom: 20,
  },
  mainTitle: {
    textAlign: 'center',
    fontSize: 25,
  },
  subTitle: {
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: 20,
  },
};
